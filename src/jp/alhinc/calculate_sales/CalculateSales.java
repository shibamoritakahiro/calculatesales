package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL = "売上ファイル名が連番になっていません";
	private static final String SALES_OVER = "合計金額が10桁を超えました";
	private static final String BRANCH_INVALID_CODE = "の支店コードが不正です";
	private static final String GOODS_INVALID_CODE = "の商品コードが不正です";
	private static final String SALES_INVALID_FORMAT = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 * @param br
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数の確認
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> goodsNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> goodsSales = new HashMap<>();
		//引数に当てはまるように変数を宣言
		String branchCharacter = "\\d{3}$";
		String commandCharacter = "^[0-9a-zA-Z]{8}$";

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, branchCharacter)) {
			return;
		}
		// 商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, goodsNames, goodsSales, commandCharacter)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)

		//処理内容2-1
		File[] files = new File(args[0]).listFiles();
 		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}+.rcd$")) {
				rcdFiles.add(files[i]);
			}

		}
		//売上ファイル名が連番になっているかの確認
		Collections.sort(rcdFiles);
		for (int i = 0; i < rcdFiles.size() - 1; i++) {

			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i +1).getName().substring(0, 8));

			if (latter - former != 1) {
				System.out.println(FILE_NOT_SERIAL);
				return;
			}
		}

		//処理内容2－2
		//ファイルを読み取り、リストに格納する
		for (int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;

			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;

				//変数を宣言する
				List<String>lines = new ArrayList<>();

				while ((line = br.readLine()) != null) {
					lines.add(line);
				}

				//売上ファイルのフォーマットが3行であるか確認
				if(lines.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + SALES_INVALID_FORMAT);

					return;
				}


				//Mapに支店コードが存在するかを確認
				if(!branchSales.containsKey(lines.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + BRANCH_INVALID_CODE);
					return;
				}
				//商品定義Mapに商品コードが存在するかを確認
				if(!goodsSales.containsKey(lines.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + GOODS_INVALID_CODE);
					return;
				}

				//売上が数字であることを確認
				if(!lines.get(2).matches("[0-9]{1,10}")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				//集計
				long fileSale = Long.parseLong(lines.get(1));
				Long saleAmount = branchSales.get(lines.get(0)) + fileSale;
				Long amount = goodsSales.get(lines.get(0)) + fileSale;


				//売上金額が10桁以内の確認
				if(amount >= 10000000000L || saleAmount >= 10000000000L) {
					System.out.println(SALES_OVER);
					return;
				}

				//Mapに格納
				branchSales.put(lines.get(0), saleAmount);
				goodsSales.put(lines.get(0), amount);

			}catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			}finally {
				//ファイルを開いてる状態
				if(br != null) {
					try {
						//ファイルを閉じる
						br.close();
					}catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}



		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, goodsNames, goodsSales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *r
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales, String branchCharacter) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//定義ファイルが存在しません。
			if(!file.exists()) {
				System.out.println(file.getName() + FILE_NOT_EXIST);
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				//定義ファイルのフォーマット確認
				if((items.length!=2)||(!items[0].matches(fileName))) {
					System.out.println(file.getName() + FILE_INVALID_FORMAT);
					return false;
				}

				//Mapに格納
				Names.put(items[0], items[1]);
				Sales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}


	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fr = new FileWriter(file);
			bw = new BufferedWriter(fr);

			//for文で変数にkeyを代入、フォーマットに当てはまるよう書き込み
			for(String branchKey : Sales.keySet()) {
				bw.write(branchKey + "," + Names.get(branchKey) + "," + Sales.get(branchKey));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
